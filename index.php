<?php include('header.php'); ?>
<div class="image-slider">
    <div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="w-100" src="https://goodchronicle.com/wp-content/uploads/2019/03/Linkedin-b2b-Lead-Generation.jpeg" alt="New York">
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://goodchronicle.com/wp-content/uploads/2019/03/Linkedin-b2b-Lead-Generation.jpeg" alt="New York">
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://goodchronicle.com/wp-content/uploads/2019/03/Linkedin-b2b-Lead-Generation.jpeg" alt="New York">
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon text-prime"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon text-prime"></span>
        </a>
    </div>
</div>
<div class="container">
    <!--Modal: modalPush-->
    <div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-notify modal-info" role="document">
            <!--Content-->
            <div class="modal-content text-center">
                <!--Header-->
                <div class="modal-header bg-teal justify-content-center">
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <i class="fa fa-bell text-teal" style="font-size:80px;" aria-hidden="true"></i>

                    <h4 class="text-center text-darkgrey">DO You Wanna To Be A Member</h4>

                </div>

                <!--Footer-->
                <div class="modal-footer flex-center">
                    <a href="signup.php" class="btn btn-teal p-5">Yes</a>
                    <a type="button" class="btn btn-teal waves-effect" data-dismiss="modal">No</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#modalPush').modal('show');
    });
</script>
<?php require_once('footer.php');?>

