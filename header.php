<!DOCTYPE html>
<html lang="en">
<head>
    <title>UserSystem</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="custom.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@700&display=swap" rel="stylesheet">
</head>
<body>
<div class="container-fluid p-0">
    <nav class="navbar navbar-expand-md header">
        <!-- Brand -->
        <a class="navbar-brand text-teal" href="index.php"><img src="http://themehats.com/themes/jango/assets/base/img/layout/logos/logo-3.png" alt="JANGO" class="c-desktop-logo-inverse"></span></a>

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="active"><a class="nav-link text-darkgrey" href="index.php">HOME</a></li>
                <li class="dropdown"><a class="nav-link dropdown-toggle text-darkgrey" data-toggle="dropdown" href="#">GALARY</a>
                    <ul class="dropdown-menu p-0" styltee="top:52px">
                        <li><a class="nav-link text-darkgrey" href="#">HOME</a></li>
                        <li><a class="nav-link text-darkgrey" href="#">SALOONS</a></li>
                        <li><a class="nav-link text-darkgrey" href="#">HAIRSTYLES</a></li>
                    </ul>
                </li>
                <li class="nav-item"><a class="nav-link text-darkgrey" href="#">ABOUT</a>
                <li class="nav-item"><a class="nav-link text-darkgrey" href="#">PORTFOLIO</a>
            </ul>
        </div>

        <div class="search-bar">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-teal my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-link"><a class="nav-link text-teal" href="signup.php"><i class="fa fa-user"></i> Sign Up</a></li>
                <li class="nav-link"><a class="nav-link text-darkgrey" href="signin.php"><i class="fa fa-sign-in"></i> Sign in</a></li>
                <li class="nav-link"><a class="nav-link text-darkgrey" href="signout.php"><i class="fa fa-sign-out"></i>Sign out</a></li>
            </ul>
        </div>
    </nav>