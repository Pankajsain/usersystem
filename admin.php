<?php
session_start();
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
    header("Location: signup.php");
}
$mysqli = mysqli_connect("localhost", "root", "root"); // Establishing Connection with Server
$mysqli->select_db("usersystem");

/*$updated = '';
if(isset($_POST['signin'])){
    $id = $_GET['id'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $gender = $_POST['gender'];
    $brandname = $_POST['brandname'];
    $sql = "UPDATE users SET firstname = '$firstname', lastname = '$lastname', email = '$email', gender = '$gender', brandname = '$brandname'  WHERE id='$id'";
    $retval = mysqli_query($mysqli, $sql);
    if (!$retval) {
        die('Could not update data: ' . mysqli_error());
        $updated = false;
    } else {
        $updated = true;
    }
    mysqli_close($mysqli);
}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>UserSystem</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="custom.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@700&display=swap" rel="stylesheet">
</head>
<body>
<div class="container-fluid p-0">
    <nav class="navbar navbar-expand-md header">
        <!-- Brand -->
        <a class="navbar-brand text-teal" href="index.php"><img src="http://themehats.com/themes/jango/assets/base/img/layout/logos/logo-3.png" alt="JANGO" class="c-desktop-logo-inverse"></span></a>

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="active"><a class="nav-link text-teal" href="admin.php">PROFILE</a></li>
                <li class="nav-item"><a class="nav-link text-darkgrey" href="usersdata.php">USERS DATA</a>
            </ul>
        </div>

        <div class="search-bar">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-teal my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
            <div class="pull-right navbar-text profile">
                <img class="profile-picture pr-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSTOxpt0mB8LHOqOazX6MB54uCDtE0driBOQE3LY-PScRprbDY1&usqp=CAU">
            </div>
            <ul class="navbar-nav">
                <li class="nav-link"><a class="nav-link text-darkgrey" href="signout.php"><i class="fa fa-sign-out"></i>Sign out</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <?php
        if($updated == true){ ?>
            <div class="alert alert-success mt-2"> <?php echo "Successfully! Updated"; ?></div>
        <?php }
        ?>
        <?php
        if(isset($_SESSION['success'])){ ?>
            <div class="alert alert-success mt-2"> <?php echo $_SESSION['success']; ?></div>
            <?php unset($_SESSION['success']);
        }
        ?>
        <?php
        $sql = "SELECT * FROM users WHERE id = '".$_SESSION['uid']."' ";
        $result = $mysqli->query($sql);
        $row = $result->fetch_assoc();
        ?>
        <div class="row">
            <div class="col-md-4">
                <div class="card mt-2">
                    <img class="card-img-top" src="https://www.w3schools.com/w3images/team2.jpg" alt="Card image" style="width:100%">
                    <div class="card-body border-teal">
                        <h4 class="card-title"><b class="text-capitalize"><?php echo $row['firstname'];?></b></h4>
                        <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                        <a href="#" class="btn btn-teal stretched-link">See Profile</a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <h2 class="form-title text-center mt-2 m-0">EDIT YOUR PERSNOL INFO</h2>
                <form class="user-signup border-teal bg-white p-2 mb-5" method="post" enctype="multipart/form-data" autocomplete="off" action="<?php echo $_SERVER['PHP_SELF'];?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fname">Firstname</label>
                                <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="firstname" value="<?php echo $row['firstname']; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lname">Lastname</label>
                                <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lastname" value="<?php echo $row['lastname']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Enter Email Address" name="email" autocomplete="nope" value="<?php echo $row['email']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="pwd">Gender:</label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group m-0">
                                            <label class="radio-inline"><input type="radio" name="gender"  <?php if($row['gender']=="male") {?> <?php echo "checked";?> <?php }?> value="male" >Male</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group m-0">
                                            <label class="radio-inline"><input type="radio" name="gender" <?php if($row['gender']=="female") {?> <?php echo "checked";?> <?php }?> value="female" >Female</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group m-0">
                                            <label class="radio-inline"><input type="radio" name="gender" <?php if($row['gender']=="other") {?> <?php echo "checked";?> <?php }?> value="other" >Other</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="brand">Brandname</label>
                                <input type="text" class="form-control" id="brandname" placeholder="Enter Your Brandname" name="brandname" value="<?php echo $row['brandname']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="d-o-b">Date Of Birth</label>
                                <input class="form-control" type="date" id="dob" name="dob" placeholder="Date" value="<?php echo $row['dob'];?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role">Select Role:</label>
                                <select class="form-control" id="role" name="role" value="<?php echo $row['role']; ?>">
                                    <option <?php if($row['role'] == 'admin' ) { echo "selected='selected'"; }?>value="admin">Admin</option>
                                    <option <?php if($row['role'] == 'user' ) { echo "selected='selected'"; }?> value="user">User</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-teal" name="edit">Submit</button>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <a class="text-teal" href="login.php">Log In Here</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $(".alert-success").delay(1000).slideUp(300);
        });
    </script>
<?php require_once('footer.php'); ?>
