<?php
session_start();
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
    header("Location: signup.php");
}
$mysqli = mysqli_connect("localhost", "root", "root"); // Establishing Connection with Server
$mysqli->select_db("usersystem");
$updated = '';
if(isset($_POST['signin'])){
    $id = $_GET['id'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $gender = $_POST['gender'];
    $brandname = $_POST['brandname'];
    $sql = "UPDATE users SET firstname = '$firstname', lastname = '$lastname', email = '$email', gender = '$gender', brandname = '$brandname'  WHERE id='$id'";
    $retval = mysqli_query($mysqli, $sql);
    if (!$retval) {
        die('Could not update data: ' . mysqli_error());
        $updated = false;
    } else {
        $updated = true;
    }
    mysqli_close($mysqli);
}
?>
<?php include('header.php'); ?>
<div class="container">
    <h3 class="text-center text-white bg-teal mt-2 mb-0">Update User Data</h3>
    <?php
    if($updated == true){ ?>
        <div class="alert alert-success mt-2"> <?php echo "Successfully! Updated"; ?></div>
    <?php }
    ?>
    <?php
    if(isset($_SESSION['success'])){ ?>
        <div class="alert alert-success mt-2"> <?php echo $_SESSION['success']; ?></div>
        <?php unset($_SESSION['success']);
    }
    ?>
    <form class="user-signup border-teal bg-white p-2 mb-5" method="post" enctype="multipart/form-data" autocomplete="off" action="<?php echo $_SERVER['PHP_SELF'];?>">
        <?php
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $sql = "SELECT * FROM users  WHERE id='$id'";
		    $result = $mysqli->query($sql);
		    $row = $result->fetch_assoc();
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fname">Firstname</label>
                        <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="firstname" value="<?php echo $row['firstname']; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lname">Lastname</label>
                        <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lastname" value="<?php echo $row['lastname']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" placeholder="Enter Email Address" name="email" autocomplete="nope" value="<?php echo $row['email']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pwd">Gender:</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label class="radio-inline"><input type="radio" name="gender"  <?php if($row['gender']=="male") {?> <?php echo "checked";?> <?php }?> value="male" >Male</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label class="radio-inline"><input type="radio" name="gender" <?php if($row['gender']=="female") {?> <?php echo "checked";?> <?php }?> value="female" >Female</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label class="radio-inline"><input type="radio" name="gender" <?php if($row['gender']=="other") {?> <?php echo "checked";?> <?php }?> value="other" >Other</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="brand">Brandname</label>
                        <input type="text" class="form-control" id="brandname" placeholder="Enter Your Brandname" name="brandname" value="<?php echo $row['brandname']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="d-o-b">Date Of Birth</label>
                        <input class="form-control" type="date" id="dob" name="dob" placeholder="Date" value="<?php echo $row['dob'];?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="role">Select Role:</label>
                        <select class="form-control" id="role" name="role" value="<?php echo $row['role']; ?>">
                            <option <?php if($row['role'] == 'admin' ) { echo "selected='selected'"; }?>value="admin">Admin</option>
                            <option <?php if($row['role'] == 'user' ) { echo "selected='selected'"; }?> value="user">User</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-teal" name="edit">Submit</button>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <a class="text-teal" href="login.php">Log In Here</a>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </form>
</div>
<script>
    $(document).ready(function(){
        $(".alert").delay(1000).slideUp(300);
    });
</script>
<?php include('footer.php'); ?>
