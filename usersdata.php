<?php
session_start();
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
    header("Location: signup.php");
}
$mysqli = mysqli_connect("localhost", "root", "root"); // Establishing Connection with Server
$mysqli->select_db("usersystem");
if($mysqli === false){
    die("ERROR: Could not connect. " . $mysqli->connect_error);
}
if(isset($_GET['delete-id']) && $_GET['delete-id'] != ''){
    $sql = "DELETE FROM users where id=".$_GET['delete-id'];
    $result = $mysqli->query($sql);
    print_r($result);
}
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>UserSystem</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="custom.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@700&display=swap" rel="stylesheet">
    </head>
<body>
<div class="container-fluid p-0">
    <nav class="navbar navbar-expand-md header">
        <!-- Brand -->
        <a class="navbar-brand text-teal" href="index.php"><img src="http://themehats.com/themes/jango/assets/base/img/layout/logos/logo-3.png" alt="JANGO" class="c-desktop-logo-inverse"></span></a>

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="active"><a class="nav-link text-teal" href="admin.php">PROFILE</a></li>
                <li class="nav-item"><a class="nav-link text-darkgrey" href="usersdata.php">USERS DATA</a>
            </ul>
        </div>

        <div class="search-bar">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-teal my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
            <div class="pull-right navbar-text profile">
                <img class="profile-picture pr-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSTOxpt0mB8LHOqOazX6MB54uCDtE0driBOQE3LY-PScRprbDY1&usqp=CAU">
            </div>
            <ul class="navbar-nav">
                <li class="nav-link"><a class="nav-link text-darkgrey" href="signout.php"><i class="fa fa-sign-out"></i>Sign out</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <?php
    if(isset($_SESSION['success'])){ ?>
        <div class="alert alert-success mt-2"> <?php echo $_SESSION['success']; ?></div>
        <?php unset($_SESSION['success']);
    }
    ?>
    <h2 class="table-title text-center m-0 mt-2">Table Data</h2>
    <?php
    /* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */


    // Attempt select query execution
    $sql = "SELECT * FROM users";
    $result = $mysqli->query($sql)
    ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Brandname</th>
            <th>Date Of Birth</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php
        while($row = $result->fetch_array()){
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['firstname'] . "</td>";
            echo "<td>" . $row['lastname'] . "</td>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['gender'] . "</td>";
            echo "<td>" . $row['brandname'] . "</td>";
            echo "<td>" . $row['dob'] . "</td>";
            echo "<td><button class=\"edit-btn\"><a class=\"text-white\" href='updateusers.php?id=".$row['id']."'><i class=\"fa fa-edit p-1\"></i>Update</a></button></td>";
            echo "<td><button class=\"delete-btn\"><a class=\"text-white\" onclick='javascript:confirmationDelete($(this));return false;' href='?delete-id=".$row['id']."'><i class=\"fa fa-remove p-1\"></i>Delete</a></button></td>";
            echo "</tr>";
        }

        ?>
        </tbody>
    </table>
</div>
</div>
<script>
    $(document).ready(function(){
        $(".alert-success").delay(1000).slideUp(300);
    });
    function confirmationDelete(anchor)
    {
        var conf = confirm('Are you sure want to delete this record?');
        if(conf)
            window.location=anchor.attr("href");
    }

    $(document).ready(function(){
        $(".alert").delay(1000).slideUp(300);
    });
</script>
<?php include('footer.php'); ?>