<?php
// define variables and set to empty values
$fnameErr = $lnameErr = $emailErr = $pswdErr = $confirmpswdErr = $genderErr = $brandnameErr = $dobErr = $roleErr = "";
$fname = $lname = $email = $pswd = $confirmpswd = $gender = $brandname = $dob = $role = "";
$errors = false;

if(isset($_POST['signup'])){
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["fname"])) {
            $fnameErr = "First Name is required";
            $errors = true;
        } else {
            $fname = test_input($_POST["fname"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$fname)) {
                $fnameErr = "Only letters and white space allowed";
            }
        }

        if (empty($_POST["lname"])) {
            $lnameErr = "Last Name is required";
            $errors = true;
        } else {
            $lname = test_input($_POST["lname"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
                $lnameErr = "Only letters and white space allowed";
            }
        }

        if (empty($_POST["email"])) {
            $emailErr = "Email is required";
            $errors = true;
        } else {
            $email = test_input($_POST["email"]);
            // check if e-mail address is well-formed
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Invalid email format";
            }
        }

        if (empty($_POST["pswd"])) {
            $pswdErr = "password is required";
            $errors = true;
        }  elseif(strlen(trim($_POST["pswd"])) < 6){
            $pswdErr = "Password must have atleast 6 characters.";}
        else {
            $pswd = test_input($_POST["pswd"]);
            // check if URL address syntax is valid
            if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/', $pswd)) {
                $pswdErr = "Invalid Password";
            }
        }
        if (empty($_POST["confirmpswd"])) {
            $confirmpswdErr = "password is required";
            $errors = true;
        } else {
            $confirmpswd = test_input($_POST["confirmpswd"]);
            // check if URL address syntax is valid
            if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/', $confirmpswd)) {
                $confirmpswdErr = "Invalid Password";
            }
            if($_POST['pswd'] != $_POST['confirmpswd']){
                $confirmpswdErr = "Password does not match";
                $errors = true;
            }
        }

        if (empty($_POST["gender"])) {
            $genderErr = "Gender is required";
            $errors = true;
        } else {
            $gender = test_input($_POST["gender"]);
        }

        if (empty($_POST["brandname"])) {
            $brandnameErr = "Brand Name is required";
            $errors = true;
        } else {
            $brandname = test_input($_POST["brandname"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$brandname)) {
                $brandnameErr = "Only letters and white space allowed";
            }
        }

        if (empty($_POST["dob"])) {
            $dobErr = "Date of Birth is required";
            $errors = true;
        }
        if (empty($_POST["role"])) {
            $roleErr = "Role is required";
            $errors = true;
        }
    }
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $pswd = md5($_POST['pswd']);
    $confirmpswd = $_POST['confirmpswd'];
    $gender = $_POST['gender'];
    $brandname = $_POST['brandname'];
    $dob = $_POST['dob'];
    $role = $_POST['role'];
    if($errors == false){
        $mysqli = mysqli_connect("localhost", "root", "root"); // Establishing Connection with Server
        $mysqli->select_db("usersystem");
        $sql = "insert into users (
	firstname, lastname, email, password, gender, brandname, dob, role) values ('$fname', '$lname', '$email', '$pswd', '$gender', '$brandname', '$dob', '$role')";
        if($mysqli->query($sql)) {
            header("Location: signin.php");
            exit;
            echo "<p>Successfully! submitted</p>";
        }else{
            echo "<h4>Not saved</h4>";
        }
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<?php require_once('header.php'); ?>
<div class="container-fluid bg-lightgrey">
    <div class="container pt-2">
        <div class="col-md-12 p-0">
            <div class="row">
                <div class="col-md-6 power-tool">
                    <img class="image-tool position-relative" src="images/jango-intro.jpg" style="width: 100%;height: 500px;">
                    <div class="fade-text">
                        <p class="tool-text text-darkgrey m-0"> JANGO is the ultimate tool to power any of your projects. Corporate, ecommerce, SAAS, CRM and much more.</p>
                        <button class="btn btn-explore">EXPLORE</button>
                    </div>
                    <div class="account-tool"><h3 class="text-account float-right pt-4">Make your account</h3></div>
                </div>
                <div class="col-md-6 signup-form">
                    <h2 class="form-title text-center m-0">Registration Form</h2>
                    <form class="user-signup border-teal bg-white p-2 mb-5" method="post" enctype="multipart/form-data" autocomplete="off" action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fname">Firstname</label>
                                    <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname" value="<?php echo $fname;?>">
                                    <div class="alert-danger mt-2"><?php echo $fnameErr;?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lname">Lastname</label>
                                    <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname" value="<?php echo $lname;?>">
                                    <div class="alert-danger mt-2"><?php echo $lnameErr;?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" placeholder="Enter Email Address" name="email" autocomplete="nope" value="<?php echo $email;?>">
                                    <div class="alert-danger mt-2"><?php echo $emailErr;?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" placeholder="Enter Your Password" name="pswd" autocomplete="off" value="<?php echo $pswd;?>">
                                    <div class="alert-danger mt-2"><?php echo $pswdErr;?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="confirm-password">Confirm Password</label>
                                    <input type="password" class="form-control" id="c-password" placeholder="Enter Your Password Again" autocomplete="off" name="confirmpswd" value="<?php echo $confirmpswd;?>">
                                    <div class="alert-danger mt-2"><?php echo $confirmpswdErr;?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="pwd">Gender:</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group m-0">
                                                <label class="radio-inline"><input type="radio" name="gender"  <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male" checked>Male</label>
                                            </div>
                                            <div class="alert-danger mt-2"><?php echo $genderErr;?></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group m-0">
                                                <label class="radio-inline"><input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female</label>
                                            </div>
                                            <div class="alert-danger mt-2"><?php echo $genderErr;?></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group m-0">
                                                <label class="radio-inline"><input type="radio" name="gender" <?php if (isset($gender) && $gender=="other") echo "checked";?> value="other">Other</label>
                                            </div>
                                            <div class="alert-danger mt-2"><?php echo $genderErr;?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="brand">Brandname</label>
                                    <input type="text" class="form-control" id="brandname" placeholder="Enter Your Brandname" name="brandname" value="<?php echo $brandname;?>">
                                    <div class="alert-danger mt-2"><?php echo $brandnameErr;?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="d-o-b">Date Of Birth</label>
                                    <input class="form-control" type="date" id="dob" name="dob" placeholder="Date" value="<?php echo $dob;?>">
                                    <div class="alert-danger mt-2"><?php echo $dobErr;?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="role">Select Role:</label>
                                    <select class="form-control" id="role" name="role">
                                        <option <?php if (isset($role) && $role=="admin") echo "checked";?> value="admin">Admin</option>
                                        <option <?php if (isset($role) && $role=="user") echo "checked";?> value="user">User</option>
                                    </select>
                                    <div class="alert-danger mt-2"><?php echo $roleErr;?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-teal" name="signup">Submit</button>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <a class="text-teal" href="signin.php">Sign In Here</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".tool-text").css({right:0,position:'absolute'});
        $(".tool-text").animate({left:-128}, 800);

        $(".btn-explore").css({right:0,top:410,position:'absolute'});
        $(".btn-explore").animate({left:-128}, 800);

        $(".image-tool").css({top:200,position:'absolute'});
        $(".image-tool").animate({top:0}, 800);

        $(".text-account").css({right:100,top:482,position:'absolute'});
        $(".text-account").animate({right:12}, 1200);

        $(".signup-form").css({top:200,top:0,position:'absolute'});
        $(".signup-form").animate({right:0}, 800);
    });
</script>
<?php require_once('footer.php'); ?>


