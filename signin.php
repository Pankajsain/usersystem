<?php
session_start();
if($errors == false){
    $mysqli = mysqli_connect("localhost", "root", "root"); // Establishing Connection with Server
    $mysqli->select_db("usersystem");
}

if(isset($_POST['signin'])){
    $email=$_POST['email'];
    $password=$_POST['password'];
    $password=md5($password); // Encrypted Password

    /*$sql = "SELECT * FROM users WHERE email='$email' and password='$password'";
    $result = $mysqli->query($sql);
    if($result->num_rows > 0){
        $userData = $result->fetch_array();
        $_SESSION['uid'] = $userData['id'];
        $_SESSION['success'] = "login successfully";
        if($role == "role"){
            header("Location: admin.php");
            exit();
        }
        elseif($role['role'] == "user"){
            header("location:user.php");
            exit();
        }
    }*/
    $sql = "SELECT * FROM users WHERE email='$email' and password='$password'";
    $result = $mysqli->query($sql);
    if($result->num_rows > 0){
        $userData = $result->fetch_array();
        $_SESSION['uid'] = $userData['id'];
        $_SESSION['success'] = "login successfully";
        header("location: profile.php");
    }
}
?>
<?php require_once('header.php'); ?>
<div class="container-fluid bg-lightgrey">
    <div class="container pt-2">
        <div class="col-md-12 p-0">
            <div class="row">
                <div class="col-md-6 power-tool">
                    <img class="image-tool position-relative" src="images/jango-intro.jpg" style="width: 100%;height: 500px;">
                    <div class="fade-text"><p class="tool-text text-darkgrey m-0"> JANGO is the ultimate tool to power any of your projects. Corporate, ecommerce, SAAS, CRM and much more.</p>
                        <button class="btn btn-explore" style="top: 82%;">EXPLORE</button></div>
                    <div class="account-tool"><h3 class="text-account float-right pt-4">Make your account</h3></div>
                </div>
                <div class="col-md-6 signin-form">
                    <h2 class="form-title text-center m-0">Signin Form</h2>
                    <form class="user-signup border-teal p-2 mb-5" method="post" enctype="multipart/form-data" autocomplete="off" action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" placeholder="Enter Email Address" name="email" autocomplete="nope">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" placeholder="Enter Your Password" name="password" autocomplete="nope">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-teal" name="signin">Submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="text-teal" href="forgotpass.php">Forgot My Password</a>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <a class="text-teal" href="signup.php">Create An Account</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".tool-text").css({right:0,position:'absolute'});
        $(".tool-text").animate({left:-128}, 800);

        $(".image-tool").css({top:200,position:'absolute'});
        $(".image-tool").animate({top:0}, 800);

        $(".btn-explore").css({right:0,position:'absolute'});
        $(".btn-explore").animate({left:-128}, 800);

        $(".text-account").css({right:100,top:482,position:'absolute'});
        $(".text-account").animate({right:12}, 1200);

        $(".signin-form").css({top:200,top:0,position:'absolute'});
        $(".signin-form").animate({right:0}, 800);
    });
</script>
<?php require_once('footer.php');?>


